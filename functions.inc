<?php

/*  __                  _   _                   _
 * / _|_   _ _ __   ___| |_(_) ___  _ __  ___  (_)_ __   ___ 
 *| |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __| | | '_ \ / __|
 *|  _| |_| | | | | (__| |_| | (_) | | | \__ \_| | | | | (__ 
 *|_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___(_)_|_| |_|\___|
 */

# Print the message, then exit with the given code
function error_exit(string $message, int $code = 1, string $caller = null) {
	header("Content-Type: text/plain; charset=us-ascii");
	print((empty($caller) ? '' : $caller . ': ') .  "$message\n");
	exit($code);
}
