<?php

/*             _               _           
 * __   _____ | |_ ___   _ __ | |__  _ __  
 * \ \ / / _ \| __/ _ \ | '_ \| '_ \| '_ \ 
 *  \ V / (_) | ||  __/_| |_) | | | | |_) |
 *   \_/ \___/ \__\___(_) .__/|_| |_| .__/ 
 *                      |_|         |_|
 */

if (empty($_GET['vote']) || empty($_POST['submit']) || $_POST['submit'] !== 'yes') {
	header("Content-Type: application/xhtml+xml; charset=utf-8");
	$xhtml = file_get_contents("html/vote.html");
	print($xhtml);
	exit(0);
}

/* Handle inserting votes here */
