<?php
/*     _ _                                     _     _
 *  __| | |__   ___ ___  _ __  _ __   ___  ___| |_  (_)_ __   ___ 
 * / _` | '_ \ / __/ _ \| '_ \| '_ \ / _ \/ __| __| | | '_ \ / __|
 *| (_| | |_) | (_| (_) | | | | | | |  __/ (__| |_ _| | | | | (__ 
 * \__,_|_.__/ \___\___/|_| |_|_| |_|\___|\___|\__(_)_|_| |_|\___|
 */

function get_dbh_connection() {
	$db_driver = 'sqlite';
	$db_name   = implode('/', array(getcwd(), 'db', 'polls.sqlite3'));
	$db_dsn    = "$db_driver:$db_name";

	try {
		$dbh       = new PDO($db_dsn);
	} catch (Exception $e) {
		die("dbconnect.inc: unable to connect: " . $e->getMessage());
	}

	$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

	return $dbh;
}

function get_error_string(&$handle) {
	$error_keys = array(
		'0. SQLSTATE error code',
		'1. Driver-specific error code',
		'2. Driver-specific error message'
	);

	return ''
		. 'Error code: ' . $handle->errorCode() . "\n"
		. "Error info: {\n"
		. implode("\n",
			  array_map(function($a) { return implode(": ", $a); },
				    array_map(null, $error_keys, $handle->errorInfo())));
}

$dbh = get_dbh_connection();
