<?php
/*                     _                        _ _         _
 *  ___ _ __ ___  __ _| |_ ___      _ __   ___ | | |  _ __ | |__  _ __  
 * / __| '__/ _ \/ _` | __/ _ \    | '_ \ / _ \| | | | '_ \| '_ \| '_ \ 
 *| (__| | |  __/ (_| | ||  __/    | |_) | (_) | | |_| |_) | | | | |_) |
 * \___|_|  \___|\__,_|\__\___|____| .__/ \___/|_|_(_) .__/|_| |_| .__/ 
 *                           |_____|_|               |_|         |_|    
 */

if (empty($_POST['name'])) {
	header("Content-Type: application/xhtml+xml; charset=utf-8");
	$xhtml = file_get_contents("html/create_poll.html");
	print($xhtml);
	exit(0);
}

require_once "functions.inc";

function get_date() {
	if (empty($_POST['date']))
		error_exit("Empty date.");

	$date = date_create($_POST['date']);
	if ($date === FALSE)
		error_exit("Invalid date: " . $_POST['date'] . ".");

	return $date;
}

function validate_date($date) {
	$now = date_create("now");
	if ($date <= $now) {
		error_exit("Date "
			   . date_format($date, "Y-m-d")
			   . " <= now ("
			   . date_format($now, "Y-m-d")
			   . ").");
	}
	return $date;
}

function get_questions() {
	if (empty($_POST['questions']))
		error_exit("questions is empty");

	$questions = $_POST['questions'];
	if (!is_array($questions))
		error_exit("questions is not an array");

	$r = array();

	foreach ($questions as $index => $question) {
		$q = trim($question);
		if (!is_string($q) || strlen($q) === 0)
			error_exit("\$questions[$index] is empty.");

		$exists = array_search($q, $r, TRUE);
		if ($exists !== FALSE)
			error_exit("\$questions[$index] ($q) is a duplicate of "
				   . "\$questions[$exists] (". $r[$exists] . ").");

		$r[$index] = $q;
	}

	return $r;
}

function get_answers($questions) {
	if (empty($_POST['answers']))
		error_exit("answers is empty");

	$answers = $_POST['answers'];
	if (!is_array($answers))
		error_exit("answers is not an array");

	$n_answers = count($answers);
	$n_questions = count($questions);

	if ($n_answers !== $n_questions) {
		error_exit("Number of [answers] ($n_answers) "
			   . " != number of questions ($n_questions)");
	}

	$r = array();

	foreach ($answers as $index => $answer_array) {
		if (!is_array($answer_array))
			error_exit("\$answers[$index] is not an array.");

		$n_as = count($answer_array);

		if ($n_as < 2)
			error_exit("\$answers[$index] has less than 2 elements.");

		$rs = array();

		foreach ($answer_array as $i => $answer) {
			$a = trim($answer);
			if (!is_string($a) || strlen($a) === 0)
				error_exit("\$answers[$index][$i] is empty.");

			$exists = array_search($a, $rs, TRUE);
			if ($exists !== FALSE)
				error_exit("\$answers[$i] ($a) is a duplicate of "
					. "\$answers[$exists] (". $rs[$exists] . ").");

			$rs[$i] = $a;
		}

		$r[$index] = $rs;
	}

	return $r;
}

$name = trim($_POST['name']);
$date = validate_date(get_date());
$questions = get_questions();
$answers = get_answers($questions);

$sql_query = <<<'EOF'
	INSERT INTO "polls" ("name", "date")
	VALUES (:name, :date)
EOF;
$sql_query = trim(preg_replace('/\s+/', ' ', $sql_query));

require_once "dbconnect.inc";

if (!$dbh->beginTransaction())
	error_exit('$dbh->beginTransaction) failed.' . "\n"
		   . get_error_string($dbh));

$sth = $dbh->prepare($sql_query);
if ($sth === FALSE)
	error_exit("\$dbh->prepare($sql_query) failed.\n"
		   . get_error_string($dbh));

if ($sth->bindValue(':name', $name, PDO::PARAM_STR) === FALSE)
	error_exit("\$sth->bindValue(':name', $name) failed.\n"
		   . get_error_string($sth));

if ($sth->bindValue(':date', date_format($date, 'U'), PDO::PARAM_INT) === FALSE)
	error_exit("\$sth->bindValue(':date', date_format($date, 'U')) failed.\n"
		   . get_error_string($sth));

if (!$sth->execute())
	error_exit("\$sth->execute() failed.\n"
		   . get_error_string($sth));
$sth = null;

$poll_id = $dbh->lastInsertId();

$add_question_query = <<<'EOF'
	INSERT INTO "questions" ("poll", "name")
	VALUES (:poll, :name)
EOF;

$sth = $dbh->prepare($add_question_query);
if ($sth === FALSE)
	error_exit("\$dbh->prepare($add_question_query) failed.\n"
		   . get_error_string($dbh));

if ($sth->bindValue(':poll', $poll_id, PDO::PARAM_INT) === FALSE)
	error_exit("\$sth->bindValue(':poll', $poll_id) failed.\n"
		   . get_error_string($sth));

$add_answer_query = <<<'EOF'
	INSERT INTO "answers" ("question", "name")
	VALUES (:question, :name)
EOF;
$sth2 = $dbh->prepare($add_answer_query);
if ($sth2 === FALSE)
	error_exit("\$dbh->prepare($add_answer_query) failed.\n"
		   . get_error_string($dbh));

foreach ($questions as $q_num => $question) {
	if ($sth->bindValue(':name', $question, PDO::PARAM_STR) === FALSE)
		error_exit("\$sth->bindValue(':name', $question) failed.\n"
			   . get_error_string($sth));
	if (!$sth->execute())
		error_exit("\$sth->execute() failed.\n"
			. get_error_string($sth));

	$question_id = $dbh->lastInsertId();

	if ($sth2->bindValue(':question', $question_id, PDO::PARAM_INT) === FALSE)
		error_exit("\$sth2->bindValue(':question', $question_id) failed.\n"
			   . get_error_string($sth2));

	foreach ($answers[$q_num] as $question_answer) {
		if ($sth2->bindValue(':name', $question_answer, PDO::PARAM_STR) === FALSE)
			error_exit("\$sth2->bindValue(':question', $question_answer) failed.\n"
				. get_error_string($sth2));
		if (!$sth2->execute())
			error_exit("\$sth2->execute() failed.\n"
			. get_error_string($sth2));
	}
}
$sth = null;
$sth2 = null;

if (!$dbh->commit())
	error_exit('$dbh->commit() failed.' . "\n"
		   . get_error_string($dbh));
$dbh = null;

header("Content-Type: text/plain; charset=utf-8");

define('PRINT_PARAMS', FALSE);

if (PRINT_PARAMS) {
	echo "Name: ";
	var_dump($name);
	print("\n");

	echo "Date: ";
	var_dump($date);
	print("\n");

	echo "Formatted date: ";
	var_dump(date_format($date, "U"));
	print("\n");

	echo "Questions: ";
	var_dump($questions);
	print("\n");

	echo "Answers: ";
	var_dump($answers);
	print("\n");

	echo '$_POST: ';
	var_dump($_POST);
	print("\n");

	echo "SQL Query: $sql_query\n";
}

echo "Inserted poll id: $poll_id.\n";
