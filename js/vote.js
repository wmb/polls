/*             _          _
 * __   _____ | |_ ___   (_)___ 
 * \ \ / / _ \| __/ _ \  | / __|
 *  \ V / (_) | ||  __/_ | \__ \
 *   \_/ \___/ \__\___(_)/ |___/
 *                     |__/     
 */

(function() {
	var VOTE_BASEURL = "vote.php";
	var GET_POLLS_BASEURL = "get_polls.php";

	function get_poll_id() {
		var url = window.location.href;
		var regex = new RegExp('[?&]poll(?:=([^&#]*)|&|#|$)')
		var results = regex.exec(url);
		if (!results) {
			console.error("Couldn't get poll id from query string.");
			return null;
		}
		if (!results[1]) {
			console.error("Couldn't get poll id from query string.");
			return '';
		}
		return decodeURIComponent(results[1].replace(/[+]/g, ' '));
	}

	function handle_answer(answer, a_ul, question_id) {
		var li = document.createElement("li");
		var id    = "answer-" + answer.id;
		var name  = "question-" + question_id;
		var value = answer.id;
		var label = document.createElement("label");
		var input = document.createElement("input");
		label.setAttribute("for", id);
		label.textContent = answer.name;
		input.setAttribute("id", id);
		input.setAttribute("name", name);
		input.setAttribute("value", value);
		input.setAttribute("type", "radio");
		input.setAttribute("required", "required");
		li.appendChild(input);
		li.appendChild(label);
		a_ul.appendChild(li);
	}

	function handle_question(question, q_ul) {
		var li = document.createElement("li");
		var p = document.createElement("p");
		p.textContent = question.name;
		li.appendChild(p);
		var a_ul = document.createElement("ul");
		question.answers.forEach(function(answer) {
			handle_answer(answer, a_ul, question.id);
		});
		li.appendChild(a_ul);
		q_ul.appendChild(li);
	}

	function show_form() {
		var poll_id = get_poll_id();
		var formdiv = document.getElementById("form-div");
		var form = document.createElement("form");
		var vote_url = encodeURI(VOTE_BASEURL + "?vote=1&poll=" + poll_id);
		form.setAttribute("method", "post");
		form.setAttribute("action", vote_url);
		form.setAttribute("enctype", "application/x-www-urlencoded");
		var submit_btn = document.createElement("input");
		var reset_btn = document.createElement("input");
		submit_btn.setAttribute("type", "submit");
		submit_btn.setAttribute("name", "submit");
		submit_btn.setAttribute("value", "yes");
		reset_btn.setAttribute("type", "reset");
		var q_ul = document.createElement("ul");
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", function(event) {
			var poll;
			try {
				var polls = JSON.parse(this.responseText);
				poll = polls.polls[0];
			} catch (e) {
				console.error('JSON.parse failed', e.message);
				console.debug('responseText', this.responseText);
				return;
			}
			console.log(JSON.stringify(poll, null, 8));
			var poll_title = document.createElement("h2");
			poll_title.textContent = poll.name;
			formdiv.appendChild(poll_title);
			poll.questions.forEach(function(question) {
				handle_question(question, q_ul);
			});
			form.appendChild(q_ul);
			form.appendChild(reset_btn);
			form.appendChild(submit_btn);
			formdiv.appendChild(form);
		});
		var requrl = encodeURI(GET_POLLS_BASEURL + "?questions=1&answers=1&poll=" + poll_id);
		xhr.open("GET", requrl, true)
		xhr.send();
	}

	window.addEventListener('load', show_form);
})();
