/*                     _                        _ _    _
 *  ___ _ __ ___  __ _| |_ ___      _ __   ___ | | |  (_)___ 
 * / __| '__/ _ \/ _` | __/ _ \    | '_ \ / _ \| | |  | / __|
 *| (__| | |  __/ (_| | ||  __/    | |_) | (_) | | |_ | \__ \
 * \___|_|  \___|\__,_|\__\___|____| .__/ \___/|_|_(_)/ |___/
 *                           |_____|_|              |__/
 */

(function() { // IIFE (immediately invoked function expression)

var ns = "http://www.w3.org/1999/xhtml";

function newElement(element) {
	return document.createElementNS(ns, element);
}

function create_input(id, name, labeltext, required=false) {
	var fragment = new DocumentFragment();
	var label    = newElement("label");
	var input    = newElement("input");

	if (id !== undefined && id !== null)
		input.setAttribute("id", id);
	input.setAttribute("name", name);

	label.setAttribute("for", id !== undefined && id !== null ? id : name);
	label.textContent = labeltext + " ";

	if (required)
		input.setAttribute("required", "required");

	fragment.appendChild(label);
	fragment.appendChild(input);
	fragment.appendChild(newElement("br"));

	return fragment;
}

function create_answer(n_question, n_answer) {
	var a_id     = "answer" + n_question.toString() + "-" + n_answer.toString();
	var a_name   = "answers[" + n_question.toString() + "][]";
	var a_label  = "Answer " + n_question.toString() + "-" + n_answer.toString();
	var fragment = create_input(a_id, a_name, a_label, true);
	var a_input  = fragment.querySelector("input");
	a_input.dataset.n_answer = n_answer;

	return fragment;
}

function get_answers_ul(q_id) {
	var q_element = document.getElementById(q_id);
	var q_li      = q_element.parentElement;
	var a_ul      = q_li.getElementsByTagNameNS(ns, "ul").item(0);
	return a_ul;
}

function get_answer_lis(q_id) {
	var a_ul      = get_answers_ul(q_id);
	var a_lis     = a_ul.getElementsByTagNameNS(ns, "li");
	return a_lis;
}

function append_answer(q_id, a_li) {
	var a_ul      = get_answers_ul(q_id);
	a_ul.appendChild(a_li);
}

function add_answer(event) {
	event.preventDefault();
	var q_id          = this.dataset.q_id;
	var n_question    = this.dataset.n_question;
	var a_lis         = get_answer_lis(q_id);
	var last_li       = a_lis.item(a_lis.length - 1);
	var last_a_input  = last_li.getElementsByTagNameNS(ns, "input").item(0);
	var last_a_number = Number(last_a_input.dataset.n_answer);
	var new_a_li      = newElement("li");
	var new_a         = create_answer(n_question, last_a_number + 1)
	new_a_li.appendChild(new_a);
	append_answer(q_id, new_a_li);
}

function create_question(n_question) {
	var q_id                   = "question" + n_question.toString();
	var q_name                 = "questions[]";
	var q_label                = "Question " + n_question.toString();

	var fragment               = create_input(q_id, q_name, q_label, true);

	var answers_title          = newElement("p");
	answers_title.textContent  = "Answers: "

	var add_btn = newElement("button");
	add_btn.setAttribute("title", "Add a new possible answer to this question");
	add_btn.textContent        = "+";
	add_btn.dataset.n_question = n_question;
	add_btn.dataset.q_id       = q_id;
	add_btn.addEventListener("click", add_answer);

	answers_title.appendChild(add_btn);
	fragment.appendChild(answers_title);

	var a_ul                   = newElement("ul");

	for (var i = 0; i < 2; ++i) {
		var a_li           = newElement("li");
		a_li.appendChild(create_answer(n_question, i));
		a_ul.appendChild(a_li);
	}

	fragment.appendChild(a_ul);
	fragment.appendChild(newElement("br"));

	return fragment;
}

function add_question(q_ul) {
	var n_question         = Number(q_ul.dataset.questions);
	var q_li               = newElement("li");
	q_li.appendChild(create_question(n_question));
	q_ul.appendChild(q_li);
	q_ul.dataset.questions = Number(n_question + 1).toString();
}

function validate_input(event) {
	// TODO: validate user input here
}

window.addEventListener("load", function() {
	var form                     = document.getElementsByTagNameNS(ns, "form").item(0);
	var q_ul                     = newElement("ul");
	var submit_btn               = newElement("input");
	var reset_btn                = newElement("input");
	var questions_title          = newElement("p");
	var add_question_btn         = newElement("button");

	add_question_btn.setAttribute("title", "Add a new question");
	add_question_btn.textContent = "+";
	add_question_btn.addEventListener("click", function(ev) {
		ev.preventDefault();
		add_question(q_ul);
	});

	questions_title.textContent  = "Questions: ";
	questions_title.appendChild(add_question_btn);

	submit_btn.setAttribute("type", "submit");
	reset_btn.setAttribute("type", "reset");

	submit_btn.addEventListener("click", validate_input);

	q_ul.dataset.questions = Number(0).toString();

	form.appendChild(questions_title);
	form.appendChild(q_ul);
	form.appendChild(reset_btn);
	form.appendChild(submit_btn);

	// Add the first question because it doesn't make sure to have a poll
	// with no questions.
	add_question_btn.click();
});

})();
