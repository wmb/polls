--              _ _                 _ 
--  _ __   ___ | | |___   ___  __ _| |
-- | '_ \ / _ \| | / __| / __|/ _` | |
-- | |_) | (_) | | \__ \_\__ \ (_| | |
-- | .__/ \___/|_|_|___(_)___/\__, |_|
-- |_|                           |_|  

PRAGMA encoding     = 'UTF-8';
PRAGMA foreign_keys = 1;

BEGIN TRANSACTION;

DROP TABLE IF EXISTS "polls";
CREATE TABLE "polls" (
       "id"        INTEGER PRIMARY KEY AUTOINCREMENT,
       "name"      TEXT    NOT NULL,
       "date"      INTEGER NOT NULL
);

DROP TABLE IF EXISTS "questions";
CREATE TABLE "questions" (
       "id"        INTEGER PRIMARY KEY AUTOINCREMENT,
       "poll"      INTEGER NOT NULL REFERENCES "polls" ("id"),
       "name"      TEXT    NOT NULL
);

DROP TABLE IF EXISTS "answers";
CREATE TABLE "answers" (
       "id"       INTEGER PRIMARY KEY AUTOINCREMENT,
       "question" INTEGER NOT NULL REFERENCES "questions" ("id"),
       "name"     TEXT    NOT NULL
);

COMMIT TRANSACTION;
