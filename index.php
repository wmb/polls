<?php

/*  _           _                   _
 * (_)_ __   __| | _____  __  _ __ | |__  _ __  
 * | | '_ \ / _` |/ _ \ \/ / | '_ \| '_ \| '_ \ 
 * | | | | | (_| |  __/>  < _| |_) | | | | |_) |
 * |_|_| |_|\__,_|\___/_/\_(_) .__/|_| |_| .__/ 
 *                           |_|         |_|
 */

header("Content-Type: application/xhtml+xml; charset=utf-8");
$xhtml = file_get_contents("html/index.html");
print($xhtml);
exit(0);
