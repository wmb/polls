/*  _           _              _
 * (_)_ __   __| | _____  __  (_)___ 
 * | | '_ \ / _` |/ _ \ \/ /  | / __|
 * | | | | | (_| |  __/>  < _ | \__ \
 * |_|_| |_|\__,_|\___/_/\_(_)/ |___/
 *                          |__/
 */

(function() {
	var VOTE_BASEURL = "vote.php";
	var GET_POLLS_BASEURL = "get_polls.php";

	function format_poll_li(document, poll) {
		var li = document.createElement("li");
		var p = document.createElement("p");
		var a = document.createElement("a");
		p.appendChild(document.createTextNode(String(poll.id) + ". "));
		a.setAttribute("href", VOTE_BASEURL + "?poll=" + encodeURIComponent(poll.id));
		a.textContent = poll.name;
		p.appendChild(a);
		p.appendChild(document.createElement("br"));
		p.appendChild(document.createTextNode("end date: " + poll.datef.slice(0, 10)));
		li.appendChild(p);
		return li;
	}

	function add_polls_to_ul(document, ul, polls) {
		console.log(polls);
		polls.polls.forEach(function(poll) {
			ul.appendChild(format_poll_li(document, poll));
		});
		document.body.appendChild(ul);
	}

	function show_polls(document) {
		var ul = document.createElement("ul");
		var xhr = new XMLHttpRequest();
		xhr.addEventListener('load', function(event) {
			var polls;
			try {
				polls = JSON.parse(this.responseText);
			} catch (e) {
				console.error('JSON.parse failed', e.message);
				console.debug('responseText', this.responseText);
				return;
			}
			add_polls_to_ul(document, ul, polls);
		});
		xhr.open("GET", GET_POLLS_BASEURL, true)
		xhr.send();
	}

	window.addEventListener('load', function(event) {
		show_polls(window.document);
	});
})();
