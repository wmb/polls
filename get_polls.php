<?php

/*            _                    _ _             _
 *  __ _  ___| |_      _ __   ___ | | |___   _ __ | |__  _ __  
 * / _` |/ _ \ __|    | '_ \ / _ \| | / __| | '_ \| '_ \| '_ \ 
 *| (_| |  __/ |_     | |_) | (_) | | \__ \_| |_) | | | | |_) |
 * \__, |\___|\__|____| .__/ \___/|_|_|___(_) .__/|_| |_| .__/ 
 * |___/        |_____|_|                   |_|         |_|
 */

require_once "functions.inc";
require_once "dbconnect.inc";

function get_results($sth) {
	$results = array();
	$colnames = array_map(function ($i) use (&$sth) {
		return $sth->getColumnMeta($i)['name'];
	}, range(0, $sth->columnCount() - 1));
	while ($row = $sth->fetch(PDO::FETCH_NUM))
		$results[] = array_combine($colnames, $row);
	return $results;
}

function get_polls(bool $all, int $poll_id = -1) {
	global $dbh;

	$query = <<<'EOF'
		SELECT	"id",
			"name",
			"date",
			datetime("date", 'unixepoch') AS "datef"
		FROM "polls"
EOF;
	if (!$all) {
		$query .= <<<'EOF'
			WHERE "date" > (CAST
				(
					strftime('%s', 'now')
					AS INTEGER
				)
			)
EOF;
	}
	if ($poll_id !== -1) {
		$query .= $all ? ' WHERE' : ' AND';
		$query .= ' "id" = ' . $dbh->quote($poll_id, PDO::PARAM_INT);
	}

	$query = trim(preg_replace('/\s+/', ' ', $query));

	$polls = $dbh->query($query);
	if ($polls === FALSE)
		error_exit("\$dbh->query($query) failed.\n"
			. get_error_string($dbh));

	return get_results($polls);
}

function get_poll_questions(int $poll_id) {
	global $dbh;

	$sql_query = 'SELECT "id", "name" FROM "questions" WHERE "poll" = :poll';
	$sth = $dbh->prepare($sql_query);
	if ($sth === FALSE)
		error_exit("\$dbh->prepare($sql_query) failed.\n"
			   . get_error_string($dbh));
	if ($sth->bindValue(':poll', $poll_id, PDO::PARAM_INT) === FALSE)
		error_exit("\$sth->bindValue(':poll', $poll_id) failed.\n"
			   . get_error_string($sth));
	if (!$sth->execute()) {
		error_exit('$sth->execute() failed.' . "\n"
			   . get_error_string($sth));
	}
	return get_results($sth);
}

function get_question_answers(int $question_id) {
	global $dbh;

	$sql_query = 'SELECT "id", "name" FROM "answers" WHERE "question" = :question';
	$sth = $dbh->prepare($sql_query);
	if ($sth === FALSE)
		error_exit("\$dbh->prepare($sql_query) failed.\n"
			   . get_error_string($dbh));
	if ($sth->bindValue(':question', $question_id, PDO::PARAM_INT) === FALSE)
		error_exit("\$sth->bindValue(':question', $question_id) failed.\n"
			   . get_error_string($sth));
	if (!$sth->execute()) {
		error_exit('$sth->execute() failed.' . "\n"
			   . get_error_string($sth));
	}
	return get_results($sth);
}

/* GET parameters:
 * - poll:
 * -- positive integer: only get this poll
 * - all:
 * -- 1: get all polls
 * -- 0: get only non-expired polls
 * - questions:
 * -- 1: also get each poll's questions
 * -- 0: don't
 * - answers:
 * -- 1: also get each question's answers
 * --- (only makes sense if questions is also set)
 * -- 0: don't
 * */

$opt_poll      = (!empty($_GET['poll']) && is_integer($_GET['poll']) && intval($_GET['poll']) > 0)
	? $_GET['poll'] : -1;
$opt_all       = !empty($_GET['all']) && boolval($_GET['all']) === TRUE;
$opt_questions = !empty($_GET['questions']) && boolval($_GET['questions']) === TRUE;
$opt_answers   = $opt_questions &&
	       !empty($_GET['answers']) && boolval($_GET['answers']) === TRUE;

$results = array();

$polls = get_polls($opt_all, $opt_poll);

if (!$opt_questions) {
	$results = $polls;
} else {
	foreach ($polls as $poll) {
		$result = array() + $poll;
		$questions = get_poll_questions($poll['id']);
		$qs = array();
		if (!$opt_answers) {
			$qs = $questions;
		} else {
			foreach ($questions as $question) {
				$q = array() + $question;
				$q['answers'] = get_question_answers($q['id']);
				$qs[] = $q;
			}
		}
		$result['questions'] = $qs;
		$results[] = $result;
	}
}

header("Content-Type: application/json");
print(json_encode(array('polls' => $results)));
